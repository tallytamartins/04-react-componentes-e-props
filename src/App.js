import React, { useState } from 'react'
import { Tela } from './components/Tela'
import { Button } from './components/Button'
import { ButtoneOperator } from './components/ButtonOperator'
import './App.css'


function App() {
  const [tela, setTela] = useState('')

  const adicionarCaracter = numero => setTela(String(tela) + String(numero))

  const igualdade = () => setTela(eval(tela))

    return (
    <div id="calculadora">
      <div id="tela">
        <Tela value={tela} disabled />
      </div>
          
          
      <div id="teclas">
  
  
        <button onClick={() => setTela('')}>C</button>
        <button onClick={() => igualdade()}>=</button>
        <button onClick={() => adicionarCaracter("/")}>/</button>
        <button onClick={() => adicionarCaracter("*")}>*</button>
        <button onClick={() => adicionarCaracter("-")}>-</button>
        <button onClick={() => adicionarCaracter("+")}>+</button>
        <Button adicionarNumero={adicionarCaracter} number={9}/>
        
      </div>
    </div>
  )
}

export default App
