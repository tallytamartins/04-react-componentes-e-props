import React from 'react'

//Componente de Função dos botões de números
export function Button({ number, adicionarNumero }) {
  return <button onClick={() => adicionarNumero(number)}>{number}</button>
}